#ifndef FILENAMESHANDLER_H
#define FILENAMESHANDLER_H
#include <QString>
#include <QDirIterator>
#include <QDir>
#include <QList>

class FileNamesHandler
{
public:
    FileNamesHandler(QString &dirPath);
    void perform();
     QList<QStringList> getListOfLists();
private:
    QString dirPath;
    const int static threadsAmount = 10;
    QList<QStringList> *listOfLists = new QList<QStringList>;
    void addElementsToListOfLists(QStringList &list);
};

#endif // FILENAMESHANDLER_H
