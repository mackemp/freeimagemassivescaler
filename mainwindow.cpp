#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->inPathButton,
            SIGNAL(clicked()),
            this,
            SLOT(on_MainWindow_openInputPathFileDialog()));
    connect(ui->outPathButton,
            SIGNAL(clicked()),
            this,
            SLOT(on_MainWindow_openOutputPathFileDialog()));
    connect(ui->scaleButton,
            SIGNAL(clicked()),
            this,
            SLOT(on_MainWindow_scaleAll()));
    ui->pixelSizeBox->setDisabled(true);
    ui->scaleByChooser->setDisabled(true);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_MainWindow_openInputPathFileDialog() {

    QString dirPath = pickDirPath();
    ui->inPath->setText(dirPath);
    currentPath = dirPath;
}

void MainWindow::on_MainWindow_openOutputPathFileDialog() {
    QString dirPath = pickDirPath();
    ui->outPath->setText(dirPath);
    currentPath = dirPath;
}

void MainWindow::on_MainWindow_scaleAll() {
  Scaler(ui->inPath->text(),
         ui->outPath->text(),
         ui->outPathCheckBox->isChecked(),
         ui->scaleByChooser->currentText(),
         ui->pixelSizeBox->text(),
         ui->autoBox->isChecked()).perform();
}

QString MainWindow::latestParentDir() {
    if(!currentPath.isNull()){
      QDir currentDir = QDir(currentPath);
      currentDir.cdUp();
      currentPath = currentDir.path();
    } else {
        currentPath = QDir::currentPath();
    }
    return currentPath;
}

QString MainWindow::pickDirPath() {
    return QFileDialog::getExistingDirectory(this, tr("Open Directory"), latestParentDir(),
                                                        QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
}
