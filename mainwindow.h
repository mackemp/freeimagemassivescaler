#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDir>
#include "scaler.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow(); 

private slots:
    void on_MainWindow_openInputPathFileDialog();
    void on_MainWindow_openOutputPathFileDialog();
    void on_MainWindow_scaleAll();

private:
    Ui::MainWindow *ui;
    QString currentPath;
    QString pickDirPath();
    QString latestParentDir();
};

#endif // MAINWINDOW_H
