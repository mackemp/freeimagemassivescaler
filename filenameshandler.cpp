#include "filenameshandler.h"

FileNamesHandler::FileNamesHandler(QString &dirPath)
{
  this->dirPath = dirPath;
}

void FileNamesHandler::perform(){
  QDir dir = QDir(dirPath);
  QStringList filters;
  filters << "*.jpg" << "*.png" << "*.bmp" << "*.jpeg";
  QStringList fileList = dir.entryList(filters);
  addElementsToListOfLists(fileList);
}

void FileNamesHandler::addElementsToListOfLists(QStringList &list) {
    QStringListIterator it(list);
    int innerListSize = list.size() / threadsAmount;
    innerListSize = innerListSize == 0 ? 1 : innerListSize;
    for(int i = 0; i < threadsAmount; ++i) {
        QStringList innerList = QStringList();
        while (it.hasNext()) {
            innerList.append(it.next());
            if(innerList.size() == innerListSize) {
                listOfLists->append(innerList);
                break;
            }
        }
    }

    for(int i = 0; i < threadsAmount; ++i) {
        while(it.hasNext()) {
             (*listOfLists)[i].append(it.next());
             break;
        }
    }
}

QList<QStringList> FileNamesHandler::getListOfLists() {
    return *listOfLists;
    delete listOfLists;
    listOfLists = NULL;
}

