#include "scaler.h"
#include "filenameshandler.h"
Scaler::Scaler(QString inputPath, QString outputPath, bool isDefaultOutputPath, QString type, QString pixelSize, bool isAuto) {
    this->inputPath = inputPath;
    this->isDefaultOutputPath = isDefaultOutputPath;
    this->type = type;
    this->pixelSize = pixelSize;
    this->outputPath = outputPath + "/";
    this->isAuto = isAuto;
}

QString Scaler::defaultOutputPath() {
   return inputPath + QString("_") + pixelSize + QString(" ") + type;
}

void Scaler::scale(const QStringList &list, QString &path) {
    QString name, fullOutputPath, fullInputPath;
    QStringListIterator it(list);
    while(it.hasNext()) {
        name = it.next();
        fullInputPath = inputPath + "/" + name;
        fullOutputPath = path + name;
        QImage image = QImage(fullInputPath);
        if(isAuto) {
          if (image.width() > image.height()) {
              image = image.scaledToWidth(1200);
          } else {
              image = image.scaledToHeight(800);
          }
        } else {
            if(type == "width"){
              image = image.scaledToWidth(pixelSize.toInt());
            } else if(type == "height") {
           image = image.scaledToHeight(pixelSize.toInt());
           }
        }
       QFile* file = new QFile(fullOutputPath);
       file->open(QIODevice::WriteOnly);
       image.save(file);
       delete file;
       file = NULL;
    }
}

QString Scaler::targetPath(const QString &name) {
      return name + "/";
}

void Scaler::perform() {
    QString path;
    if(!isDefaultOutputPath) {
       path =  outputPath;
    } else {
        path = inputPath + "_scaled_by_" + type + "_size_" + pixelSize + "/";
        QDir dir(path);
        if (!dir.exists()) {
            dir.mkpath(".");
        }
    }

    FileNamesHandler names = FileNamesHandler(inputPath);
    names.perform();
    QListIterator<QStringList> qit = QListIterator<QStringList>(names.getListOfLists());

    QFutureWatcher<void> watcher;
    QFuture<void> future;
    QList<QFuture<void>> futureList;
    while(qit.hasNext()) {
        futureList.append(QtConcurrent::run(this, &Scaler::scale, qit.next(), path));
        watcher.setFuture(future);
    }
 //   QListIterator<QFuture<void>> fit(futureList);
    for(int i = 0; i < futureList.size(); ++i) {
        futureList[i].waitForFinished();
     }
}
