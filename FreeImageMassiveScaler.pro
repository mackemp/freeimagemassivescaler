#-------------------------------------------------
#
# Project created by QtCreator 2015-12-05T14:07:44
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FreeImageMassiveScaler
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    scaler.cpp \
    filenameshandler.cpp

HEADERS  += mainwindow.h \
    scaler.h \
    filenameshandler.h

FORMS    += mainwindow.ui
QMAKE_CXXFLAGS += -std=c++11
