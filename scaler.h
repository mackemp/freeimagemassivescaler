#ifndef SCALER
#define SCALER
#include <QString>
#include <QtConcurrent/QtConcurrent>
#include <QImage>
class Scaler {
public:
    Scaler(QString inputPath, QString outputPath, bool isDefaultOutputPath, QString type, QString pixelSize, bool isAuto);
    void perform();
private:
    QString inputPath;
    QString outputPath;
    QString type;
    bool isDefaultOutputPath;
    QString pixelSize;
    bool isAuto;
    QString defaultOutputPath();
    void scale(const QStringList &path, QString &name);
    QString targetPath(const QString &name);
};

#endif // SCALER

